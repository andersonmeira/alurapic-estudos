angular.module('minhasDiretivas',[])
.directive('meuPainel', function(){

    //directive definition object
    var ddo = {};

    ddo.restric = "AE";

    ddo.scope = {
        titulo: '@titulo'
    };

    ddo.transclude = true;

    ddo.templateUrl = 'js/directives/meu-painel.html';

    return ddo;
});